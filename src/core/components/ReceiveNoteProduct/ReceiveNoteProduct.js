import React, { Component, Suspense } from 'react';
import { Link } from "react-router-dom";
import { Button, Modal } from 'react-bootstrap';
import Select from 'react-select';
import * as $ from 'jquery';
import * as transactionService from '../../services/TransactionService';
const customStyles = {
    control: styles => ({ ...styles, backgroundColor: 'white', width: '100%', })
};



class ReceiveNoteProduct extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {
            show: true,
            produectQty: [{}],
           
        }
        this.quantityOnChange = this.quantityOnChange.bind(this)
    }

    // componentDidMount = () => {
    //     this.buildRefKey()
    // }

    // componentDidUpdate = (prevProps) => {
    //     this.buildRefKey()
    // }

    // buildRefKey = () => {
    //     for (let x in this.refs) {
    //         this.refs[x].onkeypress = (e) => 
    //           this._handleKeyPress(e, this.refs[x]);
    //     }
    // }
    
    // _handleKeyPress(e, field) {
    
    //     if (e.keyCode === 13) {
    //         e.preventDefault();
    //         let arr = [];
            
    //         for (let x in this.refs) {        
    //             if(this.refs.hasOwnProperty(x)){
    //                 arr.push(x);
    //             }        
    //         }
            
    //         for(var i = 0 ; i< arr.length - 1; i++){
    //             if(this.refs[field.name].name === arr[i]){                    
    //                 this.refs[arr[i+1]].focus()
    //             }
    //         }

    //         if(this.refs[field.name].name == "btn"){
    //             this.handleHide();                
    //         }
    //     }
    // }

    // bankOnChange = (selectedOption) => {
    //     this.props.getBankOnChange(selectedOption)
    //     this.refs.btn.focus()    
    // }

    handleHide() {
        this.setState({ show: false });
        // this.props.getOrderTrackingModalKey(false)
    }

    quantityOnChange = (idx) => (event) => {
        const { name, value } = event.target;
        const produectQty = [...this.state.produectQty]
        produectQty[idx] = '';

    }

   

    render() {
        // console.log("props=>",this.props);
        return (
            <React.Fragment>                
                <div className="modal-container" >
                    <Modal
                    show={this.state.show}
                    onHide={this.handleHide}
                    container={this}
                    bsSize="large"
                    aria-labelledby="contained-modal-title-lg"
                    >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-lg">
                        Note Product
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="form-group">
                            <table id="example1" className="table table-list">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Order No.</th>
                                        <th>Item</th>
                                        <th>Unit</th>
                                        <th>order Qty.</th>
                                        <th>Pending Qty.</th>
                                        <th>Sales Qty.</th>
                                        <th>Price</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <thead>
                                    {
                                        this.props.orderProducts.productData.map((item, idx) => (
                                            <tr key={idx}>
                                                <td>{idx + 1}</td>
                                                <td>{item.entry_no}</td>
                                                <td>{item.name}</td>
                                                <td>{item.tran_unit_name}</td>
                                                <td>{item.transaction_qty}</td>
                                                <td>{item.transaction_qty}</td>
                                                <td><input 
                                                    className="form-control" 
                                                    type="text" 
                                                    onChange={this.quantityOnChange(idx)} 
                                                /></td>
                                                <td>{item.transaction_price}</td>
                                                <td>{item.creation_date}</td>
                                            </tr>
                                        ))
                                    }
                                </thead>
                            </table>
                           
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button 
                        onClick={this.handleHide}
                         className="btn btn-danger" name="btn" ref="btn">Close</button>
                    </Modal.Footer>
                    </Modal>
                </div>
            </React.Fragment>
        );
    }
}
  
export default ReceiveNoteProduct;