import React, { Component, Suspense } from 'react';
import { Link } from "react-router-dom";
import { Button, Modal } from 'react-bootstrap';
import * as transactionService from '../../services/TransactionService';
import Autocomplete from 'react-autocomplete';
import Select from 'react-select';
// import * as $ from 'jquery';
import { toast } from 'react-toastify';
const customLedgerStyles = {
    control: styles => ({ ...styles, backgroundColor: 'white', width: '100%', })
};

class GodownBatch extends Component {
    constructor(props, context) {
        super(props, context);

        this.handleHide = this.handleHide.bind(this);

        this.state = {
            show: true,
            allBatchList: [],
            goModal: true,
            manfDate:"",
            expType:{
                value:1,
                label:'Date'
            },
            expDate:"",
            expMonthValue: [
                {
                    value:'01',
                    label:'January' 
                },
                {
                    value:'02',
                    label:'Febuary' 
                },
                {
                    value:'03',
                    label:'March' 
                },
                {
                    value:'04',
                    label:'April' 
                },
                {
                    value:'05',
                    label:'May' 
                },
                {
                    value:'06',
                    label:'Jun' 
                },
                {
                    value:'07',
                    label:'July' 
                },
                {
                    value:'08',
                    label:'August' 
                },
                {
                    value:'09',
                    label:'September' 
                },
                {
                    value:'10',
                    label:'October' 
                },
                {
                    value:'11',
                    label:'November'
                },
                {
                    value:'12',
                    label:'Devember'
                }
            ],
            
            expTypeOption: [
                { value: 3, label: 'Days' },
                { value: 1, label: 'Date' },
                { value: 2, label: 'Month' },
            ]    
        };
    }

    componentDidMount = () => {

        
        this.buildRefKey()
        
        let values = [...this.props.selectedProductList];
        for(var i = 0 ; i< values[this.props.productIndex]['productGodownBatchData'].length ; i++){
            this.setState({
                ['manfDate' + i]:(values[this.props.productIndex]['productGodownBatchData'][i]['manufact_date'] != '')?values[this.props.productIndex]['productGodownBatchData'][i]['manufact_date']:"",
                ['expDate' + i]:(values[this.props.productIndex]['productGodownBatchData'][i]['exp_days'] != '')?values[this.props.productIndex]['productGodownBatchData'][i]['exp_days']:"",
                expType:{
                    value:(values[this.props.productIndex]['productGodownBatchData'][i]['exp_type_id'] == "" || values[this.props.productIndex]['productGodownBatchData'][i]['exp_type_id'] === undefined)?1:values[this.props.productIndex]['productGodownBatchData'][i]['exp_type_id'],
                    label:(values[this.props.productIndex]['productGodownBatchData'][i]['exp_type'] == "" || values[this.props.productIndex]['productGodownBatchData'][i]['exp_type'] === undefined)?'Date':values[this.props.productIndex]['productGodownBatchData'][i]['exp_type']
                }
            });
            

            values[this.props.productIndex]['productGodownBatchData'][i]['allGodownList'] = values[this.props.productIndex]['ProductAllGodownList']
            if(values[this.props.productIndex]['productGodownBatchData'][i]['godownValue'] != ''){
                this.getBatchByGodownIdAndProductId(i,values[this.props.productIndex]['productGodownBatchData'][i]['godownValue']['value'])
            }
        }
        this.setState({values})
        setTimeout(() => {
            if(this.state.goModal) {
                this.refs.godownValue0.focus();
            }
        }, 0);
    }

    componentDidUpdate = (prevProps) => {
        this.buildRefKey()          
    }

    getBatchByGodownIdAndProductId(i,id){
        var params = "?pid=" + this.props.selectedProductList[this.props.productIndex]['id'] + "&gid=" + id;
        transactionService.getBatchByGodownIdAndProductId(params).then(res => {
            var batchData = [];
            res.data.forEach(x => {
                console.log(x)
                var d = {
                    value: x.id,
                    label: x.value,
                    data: x
                }
                batchData.push(d)
            })
            let values = [...this.props.selectedProductList];
            values[this.props.productIndex]['productGodownBatchData'][i]['allBatchList'] = batchData
            this.setState({values, goModal: false}, function() {
                this.refs['batchValue'+i].focus();
            })
        })
    }

    quantityOnChange = (i,event) => {
        const re = RegExp(/^([0-9])*(\.){0,1}([0-9]){0,2}$/);
        if (event.target.value === '' || re.test(event.target.value)) {
        // if (event.target.value != '') {
            if(event.target.value.length > 1){
                var digit = event.target.value.toString()[1];
                if(digit != '.'){
                    event.target.value = event.target.value.replace(/^0+/, '');
                }            
            }
            let values = [...this.props.selectedProductList];
            if(values[this.props.productIndex]['productGodownBatchData'][i]['godownValue'] == ''){
                toast.error(`Please Select Godown`, {
                    position: "top-right",
                    autoClose: 1000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }
            else if(values[this.props.productIndex]['productGodownBatchData'][i]['batchValue'] == ''){
                toast.error(`Please Select Batch`, {
                    position: "top-right",
                    autoClose: 1000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }
            else{
                var qty_val = event.target.value
                    var available_stock = values[this.props.productIndex]['stock'] - (values[this.props.productIndex]['qty'] + (qty_val - values[this.props.productIndex]['productGodownBatchData'][i]['qty']));
                    if(event.target.value === ''){
                        console.log("productGodownBatchData")
                        values[this.props.productIndex]['productGodownBatchData'][i]['qty'] = event.target.value
                    }
                    else if(available_stock >= 0){
                        values[this.props.productIndex]['productGodownBatchData'][i]['qty'] = qty_val
                    }
                    else {                    
                        if(qty_val + available_stock > 0){
                            values[this.props.productIndex]['productGodownBatchData'][i]['qty'] = qty_val + available_stock
                        }
                        else{
                            if(this.props.tran_type == 5 || this.props.tran_type == 14 || this.props.tran_type == 12){ // sudip A Sales order
                                toast.error(`Stock is not available`, {
                                    position: "top-right",
                                    autoClose: 1000,
                                    hideProgressBar: false,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true
                                });
                            }
                            // else{
                                values[this.props.productIndex]['productGodownBatchData'][i]['qty'] = qty_val
                            // }
                            
                        }
                        
                    } 
                this.setState({values}, () => {
                    this.calculateGross(i)
                })
                // this.refs['rate'+i].focus();         
            }
        }        
        
    }

    calculateGross = (i) => {
        let values = [...this.props.selectedProductList];
        var qty = values[this.props.productIndex]['productGodownBatchData'][i]['qty']
        var rate = values[this.props.productIndex]['productGodownBatchData'][i]['rate']
        var grossTotal = 0;
        grossTotal = qty*rate;
        values[this.props.productIndex]['productGodownBatchData'][i]['grossTotal'] = parseFloat(grossTotal.toFixed(2));
        this.setState({values}, () => {
            this.props.calculateAvgQtyRate(this.props.productIndex)
        })
    }



    rateOnChange = (i,event) => {
        const re = RegExp(/^([0-9])*(\.){0,1}([0-9]){0,2}$/);
        if (event.target.value === '' || re.test(event.target.value)) {
        // if (event.target.value != '') {
            if(event.target.value.length > 1){
                var digit = event.target.value.toString()[1];
                if(digit != '.'){
                    event.target.value = event.target.value.replace(/^0+/, '');
                }            
            }
            let values = [...this.props.selectedProductList];
            if(values[this.props.productIndex]['productGodownBatchData'][i]['godownValue'] == ''){
                toast.error(`Please Select Godown`, {
                    position: "top-right",
                    autoClose: 1000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }
            else if(values[this.props.productIndex]['productGodownBatchData'][i]['batchValue'] == ''){
                toast.error(`Please Select Batch`, {
                    position: "top-right",
                    autoClose: 1000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }
            else {
                if(event.target.value === ''){
                    values[this.props.productIndex]['productGodownBatchData'][i]['rate'] = event.target.value
                }
                else{
                    values[this.props.productIndex]['productGodownBatchData'][i]['rate'] = event.target.value
                }
                this.setState({values}, () => {
                    this.calculateGross(i)
                }) 
            } 
        }        
           
    }

    handleHide() {
        let values = [...this.props.selectedProductList];
        var productGodownBatchDataList = [];
        
        if(values[this.props.productIndex]['productGodownBatchData'].length > 1){
            for(var i = 0 ; i< values[this.props.productIndex]['productGodownBatchData'].length ; i++){
                if(values[this.props.productIndex]['productGodownBatchData'][i]['qty'] > 0){
                    productGodownBatchDataList.push(values[this.props.productIndex]['productGodownBatchData'][i])                                    
                }                
                if(i == values[this.props.productIndex]['productGodownBatchData'].length - 1){
                    values[this.props.productIndex]['productGodownBatchData'] = productGodownBatchDataList
                    this.setState({ values }, () => {
                        this.props.calculateAvgQtyRate(this.props.productIndex)                    
                    })
                }
            }            
        }

        this.setState({ show: false });
        this.props.getGodownBatchModalKey(false)
        // if(this.props.wqdescindex == 1) {
        //     setTimeout(function(){ $(".wqothers").focus(); }, 500);
        // } else {
        //     setTimeout(function(){ $(".wqdiscount").focus(); }, 500);
        // }
    }

    godownOnChange = (i,selectedOption) => {
        let values = [...this.props.selectedProductList];
        if(values[this.props.productIndex]['productGodownBatchData'][i]['godownValue']['value'] != selectedOption.value){
            values[this.props.productIndex]['productGodownBatchData'][i]['godownValue'] = selectedOption
            values[this.props.productIndex]['productGodownBatchData'][i]['batchValue'] = ''
            this.setState({ values })
            this.getBatchByGodownIdAndProductId(i,selectedOption.value)
        }
        else{
            this.refs['batchValue'+i].focus();
        }        
    }

    batchOnChange = (i,selectedOption) => {
        if(this.props.tran_type == 6) {
            let values = [...this.props.selectedProductList];
            values[this.props.productIndex]['productGodownBatchData'][i]['batchValue'] = selectedOption.target.value;
            this.setState({ values }, function() {
                //this.refs['qty'+i].focus();
            })
        } else {
            let values = [...this.props.selectedProductList];
            values[this.props.productIndex]['productGodownBatchData'][i]['batchValue'] = selectedOption
            this.setState({ values }, function() {
                this.refs['qty'+i].focus();
            })
        }
        
    }

    buildRefKey = () => {
        console.log('this.refs');
        console.log(this.refs);
        for (let x in this.refs) {
            this.refs[x].onkeypress = (e) => 
              this._handleKeyPress(e, this.refs[x]);
        }
    }
    
    _handleKeyPress(e, field) {
    
        if (e.keyCode === 13) {
            e.preventDefault();
            let arr = [];
            
            for (let x in this.refs) {        
                if(this.refs.hasOwnProperty(x)){
                    arr.push(x);
                }        
            }
            
            for(var i = 0 ; i< arr.length - 1; i++){
                if(this.refs[field.name].name === arr[i]){     
                    console.log(arr[i+1]);               
                    this.refs[arr[i+1]].focus()
                }
            }

            console.log(this.refs[field.name])
        }
    }

    _godownBatchRowKeyPress = (i,e) => {
        let values = [...this.props.selectedProductList];       
        if (e.key === 'Enter' && i == this.props.selectedProductList[this.props.productIndex]['productGodownBatchData'].length - 1) {
            if(this.props.selectedProductList[this.props.productIndex]['productGodownBatchData'][i]['qty'] > 0){
                if(values[this.props.productIndex]['productGodownBatchData'][i]['godownValue'] == ''){
                    toast.error(`Please Select Godown`, {
                        position: "top-right",
                        autoClose: 1000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
                else if(values[this.props.productIndex]['productGodownBatchData'][i]['batchValue'] == ''){
                    toast.error(`Please Select Batch`, {
                        position: "top-right",
                        autoClose: 1000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
                else{
                    this.buildRefKey()
                    this.createGodownBatchRow()
                }
                
            }
            else{
                if(i == 0){
                    this.setState({ show: false });
                    this.props.getGodownBatchModalKey(false)
                }
                else{
                    this.handleHide();
                }
                
                
            }
            
        }
        
    }

    createGodownBatchRow = () => {
        let values = [...this.props.selectedProductList];
        var data = {
            godownValue: '',
            allGodownList: values[this.props.productIndex]['ProductAllGodownList'],
            batchValue: '',
            allBatchList: [],
            qty: 0,
            rate: 0,
            grossTotal: 0,
            manufact_date:"",
            exp_days:"",
            exp_type_id:"",
            exp_type:""         
        }
        
        values[this.props.productIndex]['productGodownBatchData'].push(data)
        this.setState({values});
        this.setState({
            manfDate:"",
            expDate:""
        });
    }

    removeGodownBatchRow = (selectIndex) => {
        let values = [...this.props.selectedProductList];
        values[this.props.productIndex]['productGodownBatchData'].splice(selectIndex,1)
        this.setState({ values }, () => {
            this.props.calculateAvgQtyRate(this.props.productIndex)
        })
    }
    expTypeOnChange = (i,selectedOption) =>{
        
        console.log('selectedOption');
        console.log(i);
        console.log(selectedOption);
        let selectedProductList = [...this.props.selectedProductList];
        selectedProductList[this.props.productIndex]['productGodownBatchData'][i]['exp_type_id'] = selectedOption.value;
        selectedProductList[this.props.productIndex]['productGodownBatchData'][i]['exp_type'] = selectedOption.label;
        selectedProductList[this.props.productIndex]['productGodownBatchData'][i]['exp_days'] = "";
        this.setState({expType: selectedOption,['expDate' + i]:""});
        this.refs['exp_days'+i].focus();
    }
    checkValue = (str, max) => {
        if (str.charAt(0) !== '0' || str == '00') {
          var num = parseInt(str);
          if (isNaN(num) || num <= 0 || num > max) num = 1;
          str = num > parseInt(max.toString().charAt(0)) && num.toString().length == 1 ? '0' + num : num.toString();
        };
        return str;
    }
    onFocus = (event) => {
        this.setState({
            currentRef: event.target.name
        })
    }
    expChangeMonth = (i,selectedOption) =>{
       
        if(this.state.manfDate == '' || this.state.manfDate === undefined){
            toast.error(`Please Enter Manufecturing Date`, {
                position: "top-right",
                autoClose: 1000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });
        }else if(this.state.expType.value == '' || this.state.expType.value === undefined){
            toast.error(`Please choose expiry type`, {
                position: "top-right",
                autoClose: 1000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });
        } else {
            this.setState({expDate: selectedOption});
            let selectedProductList = [...this.props.selectedProductList];
            selectedProductList[this.props.productIndex]['productGodownBatchData'][i]['exp_days'] = selectedOption.value;
        }
        
       
    }
    expChangeDate = (i,e) => {
       
        if(this.state['manfDate' + i] == '' || this.state['manfDate' + i] === undefined){
            toast.error(`Please Enter Manufecturing Date`, {
                position: "top-right",
                autoClose: 1000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });
        }else if(this.state.expType.value == '' || this.state.expType.value === undefined){
            toast.error(`Please choose expiry type`, {
                position: "top-right",
                autoClose: 1000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });
        } else {
            if(this.state.expType.value == 1){
                var input = e.target.value;
                if (/\D\/$/.test(input)) input = input.substr(0, input.length - 3);
                var values = input.split('/').map(function(v) {
                return v.replace(/\D/g, '')
                });
                if (values[0]) values[0] = this.checkValue(values[0], 31);
                if (values[1]) values[1] = this.checkValue(values[1], 12);
                
                if(values[0] > '28' && values[1] == '02'){
                    values[1] = '03'
                }    
                var output = values.map(function(v, i) {
                    return v.length == 2 && i < 2 ? v + ' / ' : v;
                });
            
                this.setState({ ['expDate' + i]: output.join('').substr(0, 14)}, function(){
                    let selectedProductList = [...this.props.selectedProductList];
                    selectedProductList[this.props.productIndex]['productGodownBatchData'][i]['exp_days'] =  output.join('').substr(0, 14);
                });
            } else {
                if(!/^[0-9]+$/.test(e.target.value)){
                    toast.error(`Please enter number only`, {
                        position: "top-right",
                        autoClose: 1000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                } else {
                    this.setState({
                        ['expDate' + i]:e.target.value     
                    });
                    let selectedProductList = [...this.props.selectedProductList];
                    selectedProductList[this.props.productIndex]['productGodownBatchData'][i]['exp_days'] = e.target.value;
                }
            }
            
            
        }
    }
    manfDateChange = (i,e) => {
        console.log("sssssss");
        // let word_With_Numbers = e.target.name;
        // let word_Without_Numbers = word_With_Numbers.replace(/[^0-9]/g, '');
        //word_Without_Numbers = word_With_Numbers.replace(word_Without_Numbers, '');
        
        
        var input = e.target.value;
        console.log(input);
        if (/\D\/$/.test(input)) input = input.substr(0, input.length - 3);
        var values = input.split('/').map(function(v) {
          return v.replace(/\D/g, '')
        });
        if (values[0]) values[0] = this.checkValue(values[0], 31);
        if (values[1]) values[1] = this.checkValue(values[1], 12);
         
        if(values[0] > '28' && values[1] == '02'){
            values[1] = '03'
        }    
        var output = values.map(function(v, i) {
            return v.length == 2 && i < 2 ? v + ' / ' : v;
        });

        
        this.setState({['manfDate' + i]: output.join('').substr(0, 14)}, function(){
            let selectedProductList = [...this.props.selectedProductList];
            selectedProductList[this.props.productIndex]['productGodownBatchData'][i]['manufact_date'] = output.join('').substr(0, 14);
        });

       
      }

    render() {
        // console.log('allGodownList');
        // console.log('this.props.tran_type');
        // console.log(this.props.tran_type);
        //console.log(this.props.selectedProductList);
        const selectedProductGodownBatchItems = this.props.selectedProductList[this.props.productIndex]['productGodownBatchData'].map((item, i) =>
            <div className="godownbatchRow" key={i}>
                <div className="row form-group">
                    <div className={this.props.tran_type == 6?"col-md-2":"col-md-3"}>
                        <Select
                            openMenuOnFocus={true}
                            value={item.godownValue}
                            onChange={this.godownOnChange.bind(this,i)}
                            options={item.allGodownList}
                            styles={customLedgerStyles}
                            placeholder="Select Godown"
                            ref={`godownValue${i}`} name={`godownValue${i}`}
                            components = {
                                {
                                    DropdownIndicator: () => null,
                                    IndicatorSeparator: () => null
                                }
                            }
                        />
                    </div>
                    <div className={this.props.tran_type == 6?"col-md-2":"col-md-3"}>
                        {
                        this.props.tran_type == 6?
                            <input type="text" ref={`batchValue${i}`} name={`batchValue${i}`}  className="form-control" placeholder="Enter Batch Value" autoComplete="off" value={item.batchValue}  onFocus={ this.onFocus.bind(this)}  onChange={this.batchOnChange.bind(this,i)}></input> 
                        :    
                        <Select
                            openMenuOnFocus={true}
                            value={item.batchValue}
                            onChange={this.batchOnChange.bind(this,i)}
                            options={item.allBatchList}
                            styles={customLedgerStyles}
                            placeholder="Select Batch"

                            ref={`batchValue${i}`} name={`batchValue${i}`}
                            components = {
                                {
                                    DropdownIndicator: () => null,
                                    IndicatorSeparator: () => null
                                }
                            }
                         
                        />
                        }
                    </div>
                    {
                        this.props.tran_type == 6 && 
                           
                        <div className="col-md-2">
                            <input type="text" ref={`manufact_date${i}`} name={`manufact_date${i}`}  className="form-control" placeholder="Manufecturing Date" autoComplete="off" value={this.state['manfDate' + i] || ""}  onFocus={ this.onFocus.bind(this)}  onChange={this.manfDateChange.bind(this,i)}></input> 
                        </div>
                    }      
                    {
                        this.props.tran_type == 6 &&   
                        <div className="col-md-1">
                            <Select
                                openMenuOnFocus={true}
                                value={{                                   
                                   
                                    value:(item.exp_type_id == "" || item.exp_type_id === undefined)?1:item.exp_type_id,
                                    label:(item.exp_type == "" || item.exp_type === undefined)?'Date':item.exp_type
                                }
                                }
                                onChange={this.expTypeOnChange.bind(this,i)}
                                options={this.state.expTypeOption}
                                styles={customLedgerStyles}
                                placeholder="Select Exp. Type"
                                ref={`exp_type${i}`} name={`exp_type${i}`}
                                components = {
                                    {
                                        DropdownIndicator: () => null,
                                        IndicatorSeparator: () => null
                                    }
                                }
                            />
                        </div>
                    } 
                    {/* {
                        this.props.tran_type == 6 && item.exp_type_id == 3 &&  
                        <div className="col-md-2">
                            <input type="text" ref={`exp_days${i}`} name={`exp_days${i}`}  className="form-control" placeholder="Expiry Days" autoComplete="off" value={item.exp_days || ""} onFocus={ this.onFocus.bind(this)} pattern="[0-9]*"  onChange={(val)=>{
                                if(this.state.manfDate == '' || this.state.manfDate === undefined){
                                    toast.error(`Please Enter Manufecturing Date`, {
                                        position: "top-right",
                                        autoClose: 1000,
                                        hideProgressBar: false,
                                        closeOnClick: true,
                                        pauseOnHover: true,
                                        draggable: true
                                    });
                                }else if(this.state.expType.value == '' || this.state.expType.value === undefined){
                                    toast.error(`Please choose expiry type`, {
                                        position: "top-right",
                                        autoClose: 1000,
                                        hideProgressBar: false,
                                        closeOnClick: true,
                                        pauseOnHover: true,
                                        draggable: true
                                    });
                                }else if(!/^[0-9]+$/.test(val.target.value)){
                                    toast.error(`Please enter number only`, {
                                        position: "top-right",
                                        autoClose: 1000,
                                        hideProgressBar: false,
                                        closeOnClick: true,
                                        pauseOnHover: true,
                                        draggable: true
                                    });
                                } else {
                                    this.setState({
                                        expDate:val.target.value     
                                    });
                                    let selectedProductList = [...this.props.selectedProductList];
                                    selectedProductList[this.props.productIndex]['productGodownBatchData'][i]['exp_days'] = val.target.value;
                                }
                                
                            }}>
                            </input> 
                            
                        </div>   
                    }   */}
                    {/* { this.props.tran_type == 6 && item.exp_type_id == 3 &&  
                        <div className="col-md-1"  style={{marginTop:"20px",left:"-18px"}}>   
                        <label>Day</label>                                    
                        </div>       
                    } 
                      && (item.exp_type_id == 1 || (item.exp_type_id == "" || item.exp_type_id === undefined)) 
                    */}
                    {
                        this.props.tran_type == 6 &&  
                        <div className="col-md-2">
                            <input type="text" ref={`exp_days${i}`} name={`exp_days${i}`}  className="form-control" placeholder="Expiry Date" autoComplete="off"  value={this.state['expDate' + i] || ""} onFocus={ this.onFocus.bind(this)} onChange={this.expChangeDate.bind(this,i)}>
                            </input> 
                        </div>                                                 
                    }
                    {/* {
                        this.props.tran_type == 6 && item.exp_type_id == 2 &&  
                        
                        <div className="col-md-2"> */}
                            {/* <Select
                                openMenuOnFocus={true}
                                value={this.state.expDate}
                                onChange={this.expChangeMonth.bind(this,i)}
                                options={this.state.expMonthValue}
                                styles={customLedgerStyles}
                                placeholder="Select Month"
                                ref={`exp_days${i}`} name={`exp_days${i}`}
                                components = {
                                    {
                                        DropdownIndicator: () => null,
                                        IndicatorSeparator: () => null
                                    }
                                }
                            /> */}

{/* <input type="text" ref={`exp_days${i}`} name={`exp_days${i}`}  className="form-control" placeholder="Expiry Month" autoComplete="off" value={item.exp_days || ""} onFocus={ this.onFocus.bind(this)} pattern="[0-9]*"  onChange={(val)=>{
                                if(this.state.manfDate == '' || this.state.manfDate === undefined){
                                    toast.error(`Please Enter Manufecturing Date`, {
                                        position: "top-right",
                                        autoClose: 1000,
                                        hideProgressBar: false,
                                        closeOnClick: true,
                                        pauseOnHover: true,
                                        draggable: true
                                    });
                                }else if(this.state.expType.value == '' || this.state.expType.value === undefined){
                                    toast.error(`Please choose expiry type`, {
                                        position: "top-right",
                                        autoClose: 1000,
                                        hideProgressBar: false,
                                        closeOnClick: true,
                                        pauseOnHover: true,
                                        draggable: true
                                    });
                                }else if(!/^[0-9]+$/.test(val.target.value)){
                                    toast.error(`Please enter number only`, {
                                        position: "top-right",
                                        autoClose: 1000,
                                        hideProgressBar: false,
                                        closeOnClick: true,
                                        pauseOnHover: true,
                                        draggable: true
                                    });
                                } else {
                                    this.setState({
                                        expDate:val.target.value     
                                    });
                                    let selectedProductList = [...this.props.selectedProductList];
                                    selectedProductList[this.props.productIndex]['productGodownBatchData'][i]['exp_days'] = val.target.value;
                                }
                                
                            }}>
                            </input> 
                            
                        </div>                                                 
                    } */}
                    {/* { this.props.tran_type == 6 && item.exp_type_id == 2 &&  
                        <div className="col-md-1" style={{marginTop:"20px",left:"-18px"}}>   
                        <label>Months</label>                                    
                        </div>       
                    } */}
                   
                    <div className={this.props.tran_type == 6?"col-md-1":"col-md-2"}>
                        <input type="text" ref={`qty${i}`} name={`qty${i}`} value={item.qty}  className="form-control" placeholder="Quantity" autoComplete="off" onChange={this.quantityOnChange.bind(this,i)}></input> 
                    </div>
                    <div className={this.props.tran_type == 6?"col-md-1":"col-md-2"}>
                        <input type="text" ref={`rate${i}`} name={`rate${i}`} value={item.rate}   className="form-control" placeholder="Rate" autoComplete="off" onChange={this.rateOnChange.bind(this,i)} onKeyPress={this._godownBatchRowKeyPress.bind(this,i)}></input>                             
                    </div>
                    <div className={this.props.tran_type == 6?"col-md-1":"col-md-2"}>
                        <label>{item.grossTotal}</label>
                        {
                            this.props.selectedProductList[this.props.productIndex]['productGodownBatchData'].length > 1 && (
                                <a onClick={() => this.removeGodownBatchRow(i)}><i className='fa fa-trash-o text-danger'></i></a>
                            )
                        }
                    </div>
                </div>
            </div>
        )
        return (
            <React.Fragment>                
                <div className="modal-container" >
                    <Modal
                    show={this.state.show}
                    onHide={this.handleHide}
                    container={this}
                    bsSize="large"
                    aria-labelledby="contained-modal-title-lg"
                    >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-lg">
                        Godown &amp; Batch
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="row">
                            <div className={this.props.tran_type == 6?"col-md-2":"col-md-3"}>
                                <label>Godown</label>
                            </div>

                            <div className={this.props.tran_type == 6?"col-md-2":"col-md-3"}>
                                <label>Batch No</label>
                            </div>
                            {
                                this.props.tran_type == 6 && 
                                <div className="col-md-2">
                                    <label>Manf. Date</label>
                                </div>  
                            }   
                            {
                                this.props.tran_type == 6 &&     
                                <div className="col-md-1">
                                    <label>Exp.Type</label>
                                </div> 
                            }
                            {
                                this.props.tran_type == 6 &&        
                                    <div className="col-md-2">
                                        <label>Expiry Date</label>
                                    </div> 
                                                          
                            }
                            <div className={this.props.tran_type == 6?"col-md-1":"col-md-2"}>
                                <label>Qty.</label>
                            </div>
                            <div className={this.props.tran_type == 6?"col-md-1":"col-md-2"}>
                                <label>Rate</label>
                            </div>
                            <div className={this.props.tran_type == 6?"col-md-1":"col-md-2"}>
                                <label>Value</label>
                            </div>
                        </div>                        
                        {selectedProductGodownBatchItems}
                    </Modal.Body>
                    {/* <Modal.Footer>
                        <Button onClick={this.handleHide}>Save</Button>
                    </Modal.Footer> */}
                    </Modal>
                </div>
            </React.Fragment>
        );
    }
}
  
export default GodownBatch;