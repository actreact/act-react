import React, { Component, Suspense } from 'react';
import { Link } from "react-router-dom";
import { Button, Modal } from 'react-bootstrap';
import Select from 'react-select';
import * as $ from 'jquery';
import * as transactionService from '../../services/TransactionService';
const customStyles = {
    control: styles => ({ ...styles, backgroundColor: 'white', width: '100%', })

};



class OrderProduct extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {
            show: true,
            produectValues: [{}]
           
        }
        this.handleHide = this.handleHide.bind(this)
        this.quantityOnChange = this.quantityOnChange.bind(this)
    }

   

    handleHide() {
        this.setState({ show: false });
        this.props.orderProductStatusChange(false)
        this.props.getProductDetails(this.state.produectValues)
    }

    quantityOnChange = (idx) => (event) => {
        const { name, value } = event.target;
        var produectValues = [...this.state.produectValues]
        produectValues[idx] = {
            index: idx,
            qty: value,
            productId: this.props.orderProducts[idx].product_id,
            stockId: this.props.orderProducts[idx].stock_id,
            entryId: this.props.orderProducts[idx].entry_id,
            orderNo: this.props.orderProducts[idx].entry_no,
            orderId: this.props.orderProducts[idx].order_id
        }
        if(value == ''){
            produectValues = produectValues.splice(idx, 1)
        }
        console.log(value)
        console.log(produectValues)
        this.setState({ produectValues })
        // this.props.getProductDetails(produectValues)

    }

   

    render() {
        // console.log("props=>",this.props);
        return (
            <React.Fragment>                
                <div className="modal-container" >
                    <Modal
                    show={this.state.show}
                    onHide={this.handleHide}
                    container={this}
                    bsSize="large"
                    aria-labelledby="contained-modal-title-lg"
                    >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-lg">
                        Order Product
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="table-responsive">
                            <table id="example1" className="table table-list">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Order No.</th>
                                        <th>Item</th>
                                        <th>Unit</th>
                                        <th>order Qty.</th>
                                        <th>Pending Qty.</th>
                                        <th>Sales Qty.</th>
                                        <th>Price</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <thead>
                                    {
                                        this.props.orderProducts.map((item, idx) => (
                                            <tr key={idx}>
                                                <td>{idx + 1}</td>
                                                <td>{item.entry_no}</td>
                                                <td>{item.name}</td>
                                                <td>{item.tran_unit_name}</td>
                                                <td>{item.order_qty}</td>
                                                <td>{item.rest_qty}</td>
                                                <td><input 
                                                    type="text"
                                                    onChange={this.quantityOnChange(idx)}
                                                /></td>
                                                <td>{item.transaction_price}</td>
                                                <td>{item.creation_date}</td>
                                            </tr>
                                        ))
                                    }
                                </thead>
                            </table>
                           
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <button 
                        onClick={this.handleHide}
                         className="btn btn-danger" name="btn" ref="btn">Close</button>
                    </Modal.Footer>
                    </Modal>
                </div>
            </React.Fragment>
        );
    }
}
  
export default OrderProduct;