import React, { Component, Suspense } from 'react';
import { Link } from "react-router-dom";
import { Button, Modal } from 'react-bootstrap';
import Select from 'react-select';
import * as $ from 'jquery';
import * as transactionService from '../../services/TransactionService';
const OrderProduct = React.lazy(() => import('../OrderProduct'));
const ReceiveNoteProduct = React.lazy(() => import('../ReceiveNoteProduct'));
const customStyles = {
    control: styles => ({ ...styles, backgroundColor: 'white', width: '100%', })
}

class OrderTrake extends Component {
    constructor(props, context) {
        super(props, context)

        this.state = {
            show: true,
            option: [
                {label : 'List', value: 1},
                {label : 'Not Applicable', value: 0}
            ],
            orderProductModalStatus: false,
            receiveNoteProductModalStatus: false,
            orderProducts: null,
            produectValues: null,
            deliveryNote: []
        }

        this.handleHide = this.handleHide.bind(this);
        this.orderTrackingOnChange = this.orderTrackingOnChange.bind(this)
        this.receiveNoteTrackingOnChange = this.receiveNoteTrackingOnChange.bind(this)
    }

    componentDidMount = () => {
        const option = {label : 'Not Applicable', value: 0}
        const deliveryNote = [...this.props.deliveryNoteRef, option]
        this.setState({deliveryNote})
    }

   
    handleHide() {
        this.setState({ show: false });
        const orderTrackObj= {
            orderTrackModalStatus:false,
            produectValues: this.state.produectValues
        }

        const receiveNoteTrackObj = {
            orderTrackModalStatus: false,
            receiveNoteRef: this.state.receiveNoteRef
        }
        
        if(this.props.transactionFlowOption == 2){
            this.props.getOrderTrackingModalKey(orderTrackObj)
        }else{
            this.props.getDeliveryNoteDetails(receiveNoteTrackObj)
        }
    }

    orderTrackingOnChange = (event) => {
        console.log(event.value)
        if(event.value == 1){
            this.setState({orderProductModalStatus: true, orderProducts: this.props.orderData})
        }
        
    }

    receiveNoteTrackingOnChange = event => {
        console.log(event.value)
        if(event.value != 0){
            // this.setState({receiveNoteProductModalStatus: true, receiveNoteProducts: this.props.receiveNoteData})
            this.setState({receiveNoteRef: event.value})
        }
    }

    getProductDetails = (data) => {
        this.setState({produectValues: data})
    }

    orderProductStatusChange = modalStatus => {
        this.setState({orderProductModalStatus: modalStatus });
    }

    render() {
        // console.log("props=>",this.props);
        return (
            <React.Fragment>                
                <div className="modal-container" >
                    <Modal
                    show={this.state.show}
                    onHide={this.handleHide}
                    container={this}
                    aria-labelledby="contained-modal-title"
                    >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title">
                        Order Tracking
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {this.props.transactionFlowOption == 2 && 
                            <div className="form-group">
                            <label>Order option</label>
                                <Select
                                    openMenuOnFocus={true}
                                    autoFocus={true}
                                    // value={this.props.bankValue}
                                    // value={ {label: this.props.wqbankName, value: this.props.bankValue} }
                                    onChange={this.orderTrackingOnChange}
                                    options={this.state.option} 
                                    styles={customStyles}
                                    // placeholder={`Select Bank`}
                                    // ref="bank" name="bank"
                                    components = {
                                        {
                                            DropdownIndicator: () => null,
                                            IndicatorSeparator: () => null
                                        }
                                    }
                                />
                            </div>
                        }
                        {this.props.transactionFlowOption == 3 && 
                            <div className="form-group">
                            <label>Delivery Note</label>
                                <Select
                                    // openMenuOnFocus={true}
                                    // autoFocus={true}
                                    //value={this.props.bankValue}
                                    // value={ {label: this.props.wqbankName, value: this.props.bankValue} }
                                    onChange={this.receiveNoteTrackingOnChange}
                                    options={this.state.deliveryNote} 
                                    styles={customStyles}
                                    // placeholder={`Select Bank`}
                                    // ref="bank" name="bank"
                                    components = {
                                        {
                                            DropdownIndicator: () => null,
                                            IndicatorSeparator: () => null
                                        }
                                    }
                                />
                            </div>
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        <button 
                        onClick={this.handleHide}
                         className="btn btn-danger" name="btn" ref="btn">Close</button>
                    </Modal.Footer>
                    </Modal>
                </div>
                {this.state.orderProductModalStatus && 
                    <OrderProduct
                        orderProducts={this.state.orderProducts}
                        getProductDetails={this.getProductDetails}
                        orderProductStatusChange= {this.orderProductStatusChange}
                    />
                }

                {this.state.receiveNoteProductModalStatus && 
                    <ReceiveNoteProduct
                        orderProducts={this.state.receiveNoteProducts}
                    />
                }
                
            </React.Fragment>
        );
    }
}
  
export default OrderTrake;